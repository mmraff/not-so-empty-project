# not-so-empty-project #

Not as empty as empty-project.

### What is this repository for? ###

* A trivial npm module for testing purposes

### Install as a Dependency ###

```sh
npm install --save not-so-empty-project
```
